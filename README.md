datalife_product_ean
====================

The product_ean module of the Tryton application platform.

[![Build Status](http://drone.datalifeit.es:8050/api/badges/datalifeit/trytond-product_ean/status.svg)](http://drone.datalifeit.es:8050/datalifeit/trytond-product_ean)

Installing
----------

See INSTALL


License
-------

See LICENSE

Copyright
---------

See COPYRIGHT
