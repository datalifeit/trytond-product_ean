# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.


def get_control_digit(num):
    checksum = 0
    for i, digit in enumerate(reversed(num)):
        checksum += int(digit) * 3 if (i % 2 == 0) else int(digit)
    return str((10 - (checksum % 10)) % 10)
