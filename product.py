# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.model import ModelSQL, fields, ValueMixin
from trytond.pool import PoolMeta, Pool
from trytond.transaction import Transaction
from .tools import get_control_digit
from sql import Literal
from sql.conditionals import Coalesce


class Configuration(metaclass=PoolMeta):
    __name__ = 'product.configuration'

    gtin14_variable = fields.MultiValue(
        fields.Char('GTIN14 logistic variable', size=1))
    sscc_variable = fields.MultiValue(
        fields.Char('SSCC logistic variable', size=1))

    @classmethod
    def default_gtin14_variable(cls, **pattern):
        return cls.multivalue_model(
            'gtin14_variable').default_gtin14_variable()

    @classmethod
    def default_sscc_variable(cls, **pattern):
        return cls.multivalue_model(
            'sscc_variable').default_gtin14_variable()

    @classmethod
    def multivalue_model(cls, field):
        pool = Pool()
        if field in {'gtin14_variable', 'sscc_variable'}:
            return pool.get('product.configuration.ean')
        return super().multivalue_model(field)


class ConfigurationEAN(ModelSQL, ValueMixin):
    '''Product Configuration EAN'''
    __name__ = 'product.configuration.ean'

    gtin14_variable = fields.Char('GTIN14 logistic variable', size=1)
    sscc_variable = fields.Char('SSCC logistic variable', size=1)

    @staticmethod
    def default_gtin14_variable():
        return '0'

    @staticmethod
    def default_sscc_variable():
        return '0'


class Product(metaclass=PoolMeta):
    __name__ = 'product.product'

    ean_identifier = fields.Function(
        fields.Many2One('product.identifier', 'EAN Identifier'),
        'get_ean_identifier', searcher='search_ean_identifier')
    ean_code = fields.Function(
        fields.Char('EAN Code'),
        'get_ean_code', searcher='search_ean_code')

    @classmethod
    def __register__(cls, module_name):
        super().__register__(module_name)

        table = cls.__table_handler__(module_name)

        if table.column_exist('ean_code'):
            pool = Pool()
            Identifier = pool.get('product.identifier')

            identifier = Identifier.__table__()
            sql_table = cls.__table__()
            cursor = Transaction().connection.cursor()

            cursor.execute(*identifier.select(
                identifier.product,
                where=identifier.type == 'ean'))
            product_ids = [r[0] for r in cursor.fetchall()]

            where = (Coalesce(sql_table.ean_code, '') != '')
            if product_ids:
                where &= ~sql_table.id.in_(product_ids)

            cursor.execute(*identifier.insert(
                columns=[
                    identifier.create_date,
                    identifier.create_uid,
                    identifier.product,
                    identifier.type,
                    identifier.code
                ],
                values=sql_table.select(
                    sql_table.create_date,
                    sql_table.create_uid,
                    sql_table.id,
                    Literal('ean'),
                    sql_table.ean_code,
                    where=where)
                ))
            table.drop_column('ean_code')

    def get_ean_identifier(self, name):
        for identifier in self.identifiers:
            if identifier.type == 'ean':
                return identifier.id

    @classmethod
    def search_ean_identifier(cls, name, clause):
        _, operator, value = clause
        domain = [
            ('identifiers', 'where', [
                    ('code', operator, value),
                    ('type', '=', 'ean'),
                    ]),
            ]
        # Add party without tax identifier
        if ((operator == '=' and value is None)
                or (operator == 'in' and None in value)):
            domain = ['OR',
                domain, [
                    ('identifiers', 'not where', [
                            ('type', '=', 'ean'),
                            ]),
                    ],
                ]
        return domain

    def get_ean_code(self, name):
        ean_identifier = self.ean_identifier
        if ean_identifier:
            return ean_identifier.code

    @classmethod
    def search_ean_code(cls, name, clause):
        return [
            ('ean_identifier.code', ) + tuple(clause[1:])
        ]

    def gtin14(self, pattern=None):
        Conf = Pool().get('product.configuration')

        if self.ean_code:
            conf = Conf(1)
            value = '%s%s' % (conf.gtin14_variable,
                self.gtin13(pattern)[:-1])
            # remove digit control of gtin13 and recompute
            return value + get_control_digit(value)
        return ''

    def gtin13(self, pattern=None):
        if self.ean_code:
            # suppose digit control is defined on ean_code
            return self.ean_code.zfill(13)
        return ''.zfill(13)


class CrossReference(metaclass=PoolMeta):
    __name__ = 'product.cross_reference'

    ean_code = fields.Char('EAN Code')


class ProductCrossReference(metaclass=PoolMeta):
    __name__ = 'product.product'

    def gtin13(self, pattern=None):
        cross_reference = self.get_cross_reference(pattern)
        if cross_reference and cross_reference.ean_code:
            return cross_reference.ean_code.zfill(13)
        return super().gtin13(pattern)
